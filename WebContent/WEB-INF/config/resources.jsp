
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> --%>

<!-- Custom CSS -->
<c:url value='/resources/css/main.css' var="customStyleCSS" />

<!-- Vendors -->
<c:url value='/resources/lib/bootstrap/dist/css/bootstrap.min.css' var="BootstrapCSS" />
<c:url value='/resources/lib/bootstrap/dist/js/bootstrap.min.js' var="BootstrapJS" />
<c:url value='/resources/lib/jquery/dist/jquery.min.js' var="JQueryJS" />
<c:set value="https://fonts.googleapis.com/icon?family=Material+Icons" var="GoogleMaterialIcons" />

<!-- Images -->
<c:url value='/resources/assets/img/logobf280.png' var="logoHome" />
<c:url value='/resources/assets/img/logobf80.png' var="logoNavBar" />

<!-- Icon -->
<c:url value='/resources/assets/img/fastandfood.ico' var="iconBF" />