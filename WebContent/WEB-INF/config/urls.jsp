<!-- URls Frontend -->
<!-- Navbar -->
<c:url value="/home" var="homeUrl" />
<c:url value="/products" var="productsUrl" />
<c:url value="/contact" var="contactUrl" />

<!-- URls Backend -->
<c:url value="/admin/products/list" var="adminProductsListUrl" />
<c:url value="/admin/products/create" var="adminProductsCreateUrl" />
<c:url value="/admin/products/get" var="adminProductsGetUrl" />
<c:url value="/admin/products/edit" var="adminProductsEditUrl" />
<c:url value="/admin/products/delete" var="adminProductsDeleteUrl" />
