<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html lang="fr">



  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Renald SOUDER">
    <link rel="icon" href="${iconBF}">
	
    <title>Fast and Food - Accueil</title>
	
	<!-- Materials icons -->
	<link href="${GoogleMaterialIcons}" rel="stylesheet">
	
    <!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="${BootstrapCSS}" />
		<link rel="stylesheet" href="${customStyleCSS}" />

    <!-- Custom styles for this template -->
    <link href="cover.css" rel="stylesheet">
  </head>

  <body class="text-center">
  
	
	<c:choose>
		<c:when test="${ isHome }">
			<!-- Don't show nav bar -->
		</c:when>
		<c:otherwise>
			<%@ include file="navbar.jsp" %>
			
		<c:choose>
			<c:when test="${ !empty user }">
				<p>Bienvenue ${ user.firstname } !</p>
				<p><a href="<c:url value='/logout' />">Se déconnecter</a></p>
			</c:when>
			<c:otherwise>
				<p>
					<a href="<c:url value='/signin' />">S'inscrire</a>
					<a href="<c:url value='/login' />">Se connecter</a>
				</p>
			</c:otherwise>
		</c:choose>
			
		</c:otherwise>
	</c:choose>