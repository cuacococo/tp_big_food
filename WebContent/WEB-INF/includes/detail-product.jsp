<div class="row">
	<div class="col-md-6">
		<div class="card mx-auto" style="width: 18rem;">
		  <img class="card-img-top mt-3" src="<c:url value='${product.img}'/>" alt="Image du produit">
		  <div class="card-body text-center">
		    <a href="" class="btn btn-info"><i class="mt-2 material-icons">share</i></a>
		  </div>
		</div>
	</div>
	<div class="col-md-6 mt-3 book-detail text-center mx-auto">
		<h3 class="book-title">${product.title}</h3>
		<p>
			<strong>R�sum� :</strong><br />
			${product.description}
		</p>
		<p>
	
	<c:choose>     
         <c:when test = "${product.idProductType == 1}">
            <strong>Type de produit : Burger</strong>
         </c:when>
         
         <c:when test = "${product.idProductType == 2}">
          	 <strong>Type de produit : Plat</strong>
         </c:when>
         
         <c:when test = "${product.idProductType == 3}">
          	 <strong>Type de produit : Dessert</strong>
         </c:when>
         <c:otherwise>
            
         </c:otherwise>
      </c:choose>

			<br />
		<c:choose>     
         <c:when test = "${product.idStore == 1}">
            <strong>Magasin : Montpellier</strong>
         </c:when>
         
         <c:when test = "${product.idStore == 2}">
          	 <strong>Magasin : Paris</strong>
         </c:when>
         
         <c:when test = "${product.idStore == 3}">
          	 <strong>Magasin : Marseille</strong>
         </c:when>
         <c:otherwise>
            
         </c:otherwise>
      </c:choose>
			
			<br />
			<strong>Prix :</strong> ${product.price}
		</p>
<%-- 		<c:if test="${product.available}"><div class="badge badge-success">Disponible</div></c:if> --%>
<%-- 		<c:if test="${!product.available}"><div class="badge badge-danger">Rupture</div></c:if> --%>
	</div>
</div>