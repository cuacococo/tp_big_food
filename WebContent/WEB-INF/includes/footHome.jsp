		<footer class="mastfoot mt-auto">
			<div class="inner">
				<p>Big Food web site by <a href="http://www.microindigo.com">Micro Indigo</a></p>
				<p>&copy; <fmt:formatDate value="<%= new java.util.Date() %>" pattern="YYYY" /> Copyright</p>
			</div>
			
		</footer>
	</div>
	</body>
	</html>