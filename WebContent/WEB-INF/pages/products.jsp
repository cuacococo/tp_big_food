<c:if test="${ backend && reload }">
	<c:redirect url="/admin/products/list" />
</c:if>

<c:if test="${ idCity == 1 }">
            <c:out value="Montpellier!"/>
</c:if>


<c:choose>
	<c:when test="${ !empty products }">
		<%@ include file="/WEB-INF/includes/list-products.jsp" %>
	</c:when>
	<c:when test="${ empty products }">
		<p class="text-center m-4 border p-4">Il n'y a pas de produit en base de donn�es.</p>
	</c:when>
	<c:otherwise>
		<p class="text-center m-4 border p-4">Vous ne pouvez acc�der au composant.</p>
	</c:otherwise>
</c:choose>