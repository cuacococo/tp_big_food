

	<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
		<header class="masthead mb-auto">
		<div class="mt-6">
			<img src="${logoHome}" alt="Logo" height="240px" width="240px" />
		</div>
		
		<div class="mt-6">
		<p class="lead mt-6">Sélectionner une ville</p>
		<p class="lead">

			<a href="${productsUrl}" class="btn btn-lg btn-default">MONTPELLIER </a> 
			<a href="#" class="btn btn-lg btn-default">PARIS</a>
			<a href="#" class="btn btn-lg btn-default">MARSEILLE</a>
		</p>
		</div>
	</header>

		<main role="main" class="inner cover">

		</main>
