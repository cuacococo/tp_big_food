<c:if test="${ backend && reload }">
	<c:redirect url="/admin/products/list" />
</c:if>

<div class="row mt-3">
<form action="" method="POST" id="product-form"></form>
	<input name="id" type="hidden" value="${product.id}" />
	
	<div class="form-group col-md-4 offset-1">
		<input name="title" type="text" value="${product.title}" placeholder="Titre" class="form-control form-control-sm" form="product-form" required />
		<br />
		<input name="img" type="text" value="${product.img}" placeholder="Image" class="form-control form-control-sm" form="product-form" />
		<br />
		<input name="price" type="text" value="${product.price}" placeholder="Prix" class="form-control form-control-sm" form="product-form" required />
		<br />
		<input type=text list=productType value="${product.idProductType}" placeholder="Type de produit" class="form-control form-control-sm" form="product-form" required>
    <datalist id=productType >
       <option value="1"> Burger </option>
       <option value="2"> Plat </option>
       <option value="3"> Dessert </option>

    </datalist>

		
<%-- 		Disponible en stock ? <input name="available" type="checkbox" value="${book.available}" checked="checked" form="product-form" /> --%>
	</div>
	
	<div class="form-group col-md-4 offset-1">
		<textarea name="description" placeholder="Description" cols="20" rows="5" class="form-control form-control-sm" form="product-form">${product.description}</textarea>
		<br />
		<button type="submit" class="btn btn-sm btn-info" form="product-form">Valider</button>
	</div>
</div>