package fr.mi.web.servlet;


import static fr.mi.web.util.Constants.HOME_TAB_URL;
import static fr.mi.web.util.Constants.PRODUCTS_TAB_URL;	
import static fr.mi.web.util.Constants.FRONT_SERVLET_NAME;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.mi.web.util.ActionManager;

@WebServlet(
	name=FRONT_SERVLET_NAME,
	urlPatterns={
		HOME_TAB_URL,
		PRODUCTS_TAB_URL })
public class FrontServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String jspName = ActionManager
			.getAction(request)
			.executeAction(request);
		

		getServletContext()
			.getNamedDispatcher(jspName)
			.forward(request, response);
	}
}











