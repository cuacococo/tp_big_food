package fr.mi.web.util;

import static fr.mi.web.util.Constants.*;

public enum NavigationTabs {
	// Frontend
	HOME(HOME_TAB_LABEL, HOME_TAB_URL, false),
	PRODUCTS(PRODUCTS_TAB_LABEL, PRODUCTS_TAB_URL, false),
	CONTACT(CONTACT_TAB_LABEL, CONTACT_TAB_URL, false),
	
	// Backend
	PRODUCTS_LIST(ADMIN_PRODUCTS_LIST_LABEL, ADMIN_PRODUCTS_LIST_URL, true),
	PRODUCTS_CREATE(ADMIN_PRODUCTS_CREATE_LABEL, ADMIN_PRODUCTS_CREATE_URL, true),
	PRODUCTS_EDIT(ADMIN_PRODUCTS_EDIT_LABEL, ADMIN_PRODUCTS_EDIT_URL, true);
//	Attention ,,, apr�s chauqe ligne puis ; pour terminer
	
	private String label;
	private String url;
	private boolean adminTab;
	private boolean active;
	
	private NavigationTabs(String label, String url, boolean adminTab) {
		this.label = label;
		this.url = url;
		this.adminTab = adminTab;
	}
	
	public String getLabel() { return label; }
	public String getUrl() { return url; }
	public boolean isAdminTab() { return adminTab; }
	public boolean isActive() { return active; }
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public void setUrlWithCtxPath(String ctxPath) {
		this.url = ctxPath + url;
	}
}