package fr.mi.web.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import fr.mi.business.HomeAction;
import fr.mi.business.SigninAction;
import fr.mi.business.LoginAction;
import fr.mi.business.LogoutAction;
import fr.mi.business.ProductsAction;
import fr.mi.business.ContactAction;
import fr.mi.business.CreateProductAction;
import fr.mi.business.DeleteProductAction;
import fr.mi.business.EditProductAction;
import fr.mi.business.GetProductAction;


public final class ActionManager {
	private static Map<String, AbstractAction> actions = new HashMap<>();
	
	// Frontend
	private static final String ACTION_HOME = Constants.HOME_TAB_ACTION;
	private static final String ACTION_PRODUCTS = Constants.PRODUCTS_TAB_ACTION;
	private static final String ACTION_CONTACT = Constants.CONTACT_TAB_ACTION;
	
	// Backend
	private static final String ACTION_ADMIN_LIST_PRODUCTS = "list";
	private static final String ACTION_ADMIN_GET_PRODUCT = "get";
	private static final String ACTION_ADMIN_CREATE_PRODUCT = "create";
	private static final String ACTION_ADMIN_EDIT_PRODUCT = "edit";
	private static final String ACTION_ADMIN_DELETE_PRODUCT = "delete";
	
	private static final String ACTION_SIGNIN = Constants.ACTION_SIGNIN;
	private static final String ACTION_LOGIN = Constants.ACTION_LOGIN;
	private static final String ACTION_LOGOUT = Constants.ACTION_LOGOUT;
	
	private ActionManager() {}
	
	static {
		// Frontend
		actions.put(ACTION_HOME, new HomeAction());
		actions.put(ACTION_PRODUCTS, new ProductsAction());
		actions.put(ACTION_CONTACT, new ContactAction());
		
		// Backend
		actions.put(ACTION_ADMIN_LIST_PRODUCTS, new ProductsAction());
		actions.put(ACTION_ADMIN_GET_PRODUCT, new GetProductAction());
		actions.put(ACTION_ADMIN_CREATE_PRODUCT, new CreateProductAction());
		actions.put(ACTION_ADMIN_EDIT_PRODUCT, new EditProductAction());
		actions.put(ACTION_ADMIN_DELETE_PRODUCT, new DeleteProductAction());
		
		actions.put(ACTION_SIGNIN, new SigninAction());
		actions.put(ACTION_LOGIN, new LoginAction());
		actions.put(ACTION_LOGOUT, new LogoutAction());
	}
	
	public static AbstractAction getAction(HttpServletRequest request) {
		String actionName = getActionName(request);
		return actions.get(actionName);
	}
	
	public static String getActionName(HttpServletRequest request) {
		String uri = request.getRequestURI();
		return uri.substring(uri.lastIndexOf("/")+1);
	}
}
