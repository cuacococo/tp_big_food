package fr.mi.web.util;

public final class Constants {
	// Servlets names
	public static final String FRONT_SERVLET_NAME = "FrontServlet";
	public static final String BACK_SERVLET_NAME = "BackServlet";
	
	// Nav tabs labels / actions name
	public static final String HOME_TAB_LABEL = "Accueil";
	public static final String PRODUCTS_TAB_LABEL = "Liste des produits";
	public static final String CONTACT_TAB_LABEL = "Contact";
	public static final String ADMIN_PRODUCTS_LIST_LABEL = PRODUCTS_TAB_LABEL;
	public static final String ADMIN_PRODUCTS_CREATE_LABEL = "Ajouter un produit";
	public static final String ADMIN_PRODUCTS_EDIT_LABEL = "Editer d'un produit";
	
	// Frontend action names
	public static final String HOME_TAB_ACTION = "home";
	public static final String PRODUCTS_TAB_ACTION = "products";
	public static final String CONTACT_TAB_ACTION = "contact";

	
	// Backend action names
	public static final String ACTION_ADMIN_LIST_PRODUCTS = "list";
	public static final String ACTION_ADMIN_CREATE_PRODUCT = "create";
	public static final String ACTION_ADMIN_GET_PRODUCT = "get";
	public static final String ACTION_ADMIN_EDIT_PRODUCT = "edit";
	public static final String ACTION_ADMIN_DELETE_PRODUCT = "delete";
	
	// Access action names
	public static final String ACTION_SIGNIN = "signin";
	public static final String ACTION_LOGIN = "login";
	public static final String ACTION_LOGOUT = "logout";
	
	// Nav tabs urls
	public static final String HOME_TAB_URL = "/"+HOME_TAB_ACTION;
	public static final String PRODUCTS_TAB_URL = "/"+PRODUCTS_TAB_ACTION;
	public static final String CONTACT_TAB_URL = "/"+CONTACT_TAB_ACTION;

	
	// Backend Urls
	public static final String ADMIN_PRODUCTS_BASEURL = "/admin/products/";
	public static final String ADMIN_PRODUCTS_LIST_URL = ADMIN_PRODUCTS_BASEURL+ACTION_ADMIN_LIST_PRODUCTS;
	public static final String ADMIN_PRODUCTS_CREATE_URL = ADMIN_PRODUCTS_BASEURL+ACTION_ADMIN_CREATE_PRODUCT;
	public static final String ADMIN_PRODUCTS_GET_URL = ADMIN_PRODUCTS_BASEURL+ACTION_ADMIN_GET_PRODUCT;
	public static final String ADMIN_PRODUCTS_EDIT_URL = ADMIN_PRODUCTS_BASEURL+ACTION_ADMIN_EDIT_PRODUCT;
	public static final String ADMIN_PRODUCTS_DELETE_URL = ADMIN_PRODUCTS_BASEURL+ACTION_ADMIN_DELETE_PRODUCT;
	
	// Access urls
	public static final String SIGNIN_URL = "/"+ACTION_SIGNIN;
	public static final String LOGIN_URL = "/"+ACTION_LOGIN;
	public static final String LOGOUT_URL = "/"+ACTION_LOGOUT;
	
	// Jsp names
	public static final String JSP_HOME_NAME = "HomeJSP";
	public static final String JSP_PRODUCTS_NAME = "ProductsJSP";
	public static final String JSP_PRODUCT_NAME = "ProductJSP";
	public static final String JSP_PRODUCT_FORM_NAME = "ProductFormJSP";
	


	public static final String JSP_CONTACT_NAME = "ContactJSP";
	public static final String JSP_SIGNIN_NAME = "SigninJSP";
	public static final String JSP_LOGIN_NAME = "LoginJSP";
	public static final String JSP_LOGOUT_NAME = "LogoutJSP";
	
	// Titles pages
	public static final String TITLE_HOME_PAGE = "Page d'accueil";
	public static final String TITLE_PRODUCTS_PAGE = "Liste des produits Montpellier";
	public static final String TITLE_PRODUCT_ADD_PAGE = "Cr�ation d'un produit";
	public static final String TITLE_PRODUCT_EDIT_PAGE = "Edition d'un produit";
	public static final String TITLE_CONTACT_PAGE = "Contactez-nous !";
	public static final String TITLE_SIGNIN_PAGE = "Inscription";
	public static final String TITLE_LOGIN_PAGE = "Connexion";

	
	// Session : info messages
	public static final String INFO_ATTR = "info";
	public static final String INFO_PRODUCT_CREATED_OK = "Le produit a bien �t� cr��";
	public static final String INFO_PRODUCT_CREATED_FAIL = "Le produit n'a pas pu �tre cr��";
	public static final String INFO_PRODUCT_UPDATED_OK = "Le produit a bien �t� mis � jour";
	public static final String INFO_PRODUCT_UPDATED_FAIL = "Le produit n'a pas pu �tre mis � jour";
	
	// JAVABEANS
	// Book attributes
	public static final String PRODUCT_ID = "id";
	public static final String PRODUCT_TITLE = "title";
	public static final String PRODUCT_DESCRIPTION = "description";
	public static final String PRODUCT_PRICE = "price";
	public static final String PRODUCT_AVAILABLE = "available";
	public static final String PRODUCT_IMG = "img";
	public static final String PRODUCT_ID_STORE = "idStore";
	public static final String PRODUCT_ID_PRODUCT_TYPE = "idProductType";
	
	private Constants() {}
}
