package fr.mi.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import fr.mi.model.beans.Product;
import fr.mi.web.util.JpaUtil;

public class ProductDAO implements InterfaceDAO<Product> {
	private EntityManager em;
	
	public ProductDAO() {
		em = JpaUtil.getEntityManager();
	}

	@Override
	public List<Product> findAll() {
		TypedQuery<Product> query = em.createQuery("from Product", Product.class);
		List<Product> products = query.getResultList();
		em.close();
		return products;
	}

	@Override
	public Product find(int id) {
		Product product = em.find(Product.class, id);
		em.close();
		return product;
	}

	@Override
	public void delete(int id) { executeUpdate(em.find(Product.class, id), true); }

	@Override
	public void update(Product product) { executeUpdate(em.merge(product), false); }

	@Override
	public void create(Product product) { executeUpdate(product, false); }
	
	private void  executeUpdate(Product product, boolean remove) {
		EntityTransaction transaction = em.getTransaction();
		
		try {
			transaction.begin();
			if (!remove) em.persist(product);
			else em.remove(product);
			transaction.commit();
		}
		catch (Exception e) { transaction.rollback(); }
		finally { if (em.isOpen()) em.close(); }
	}
}
