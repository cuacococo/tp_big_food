package fr.mi.model.dao;

import fr.mi.model.beans.Product;
import fr.mi.model.beans.User;

public final class DAOFactory {
	private DAOFactory() {}
	
	public static InterfaceDAO<Product> getProductDAO() {
		return new ProductDAO();
	}
	public static InterfaceDAO<User> getUserDAO() {
		return new UserDAO();
	}
	
}
