package fr.mi.model.beans;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product implements Serializable {
	private final static long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(length=90, nullable=false)
	private String title;
	@Column(columnDefinition="TEXT")
	private String description;
	@Column(columnDefinition="DECIMAL(5,2)")
	private double price;
	@Column(length=90, nullable=false)
	private String img;
	@Column(length=11, nullable=true)
	private int idStore;
	@Column(length=11, nullable=true)
	private int idProductType;
	
	public Product() {}
	public Product(
		String title,
		String description,
		double price,
		String img,
		int idStore,
		int idProductType) {
			this.title = title;
			this.description = description;
			this.price = price;
			this.setImg(img);
			this.idStore = idStore;
			this.idProductType = idProductType;
	}
	public Product(
			int id,
			String title,
			String description,
			double price,
			String img,
			int idStore,
			int idProductType) {
		this(title, description, price, img, idStore, idProductType);
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		if (img.isEmpty()) img = "no-img.png";
		this.img = img.contains("resources") ? img : "/resources/assets/img/" + img;
	}
	public int getIdStore() {
		return idStore;
	}
	public void setIdStore(int idStore) {
		this.idStore = idStore;
	}
	public int getIdProductType() {
		return idProductType;
	}
	public void setIdProductType(int idProductType) {
		this.idProductType = idProductType;
	}
	
}
