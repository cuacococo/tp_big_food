package fr.mi.business;

import javax.servlet.http.HttpServletRequest;

import fr.mi.model.beans.Product;
import fr.mi.model.dao.DAOFactory;
import fr.mi.web.util.AbstractAction;
import fr.mi.web.util.Constants;

public class GetProductAction extends AbstractAction {
	private static final String JSP_PAGE = Constants.JSP_PRODUCT_NAME;
	private static final String TITLE = Constants.TITLE_PRODUCTS_PAGE;
	
	@Override
	public String executeAction(HttpServletRequest request) {
		request.setAttribute("title", TITLE);
		
		String idStr = request.getParameter("id");
		if (idStr != null) {
			try {
				int id = Integer.parseInt(idStr);
				Product product = DAOFactory.getProductDAO().find(id);
				request.setAttribute("product", product);
			}
			catch (Exception e) { e.printStackTrace(); }
		}
		
		return JSP_PAGE;
	}

}
