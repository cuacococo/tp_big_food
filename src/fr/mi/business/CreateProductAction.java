package fr.mi.business;

import static fr.mi.web.util.Constants.*;

import javax.servlet.http.HttpServletRequest;

import fr.mi.model.beans.Product;
import fr.mi.model.dao.DAOFactory;
import fr.mi.web.util.AbstractAction;
import fr.mi.web.util.Constants;

public class CreateProductAction extends AbstractAction {
	private static final String JSP_PAGE = Constants.JSP_PRODUCT_FORM_NAME;
	private static final String TITLE = Constants.TITLE_PRODUCT_ADD_PAGE;
	
	@Override
	public String executeAction(HttpServletRequest request) {
		request.setAttribute("title", TITLE);
		
		
		if (request.getMethod().equals("POST")) {
			String title = request.getParameter(PRODUCT_TITLE);
			String description = request.getParameter(PRODUCT_DESCRIPTION);
			String priceStr = request.getParameter(PRODUCT_PRICE);
			String img = request.getParameter(PRODUCT_IMG);
			String id_Store = request.getParameter(PRODUCT_ID_STORE);
			String id_ProductType = request.getParameter(PRODUCT_ID_PRODUCT_TYPE);
			
			try {
				priceStr = priceStr.replace(",", ".");
				double price = Double.parseDouble(priceStr);
				int idStore = Integer.parseInt(id_Store);	
				int idProductType = Integer.parseInt(id_ProductType);	
						
				Product product = new Product(title, description, price, img, idStore, idProductType);
				DAOFactory.getProductDAO().create(product);
				request.getSession().setAttribute(INFO_ATTR, INFO_PRODUCT_CREATED_OK);
				request.setAttribute("redirectUrl", ADMIN_PRODUCTS_LIST_URL);
			}
			catch (Exception e) {
				request.getSession().setAttribute(INFO_ATTR, INFO_PRODUCT_CREATED_FAIL);
				request.setAttribute("redirectUrl", ADMIN_PRODUCTS_CREATE_URL);
				e.printStackTrace();
			}
		}
		
		return JSP_PAGE;
	}

}