package fr.mi.business;

import javax.servlet.http.HttpServletRequest;

import fr.mi.model.beans.Product;
import fr.mi.model.dao.DAOFactory;
import fr.mi.web.util.AbstractAction;
import static fr.mi.web.util.Constants.*;

public class EditProductAction extends AbstractAction {
	private static final String JSP_PAGE = JSP_PRODUCT_FORM_NAME;
	private static final String TITLE = TITLE_PRODUCT_EDIT_PAGE;
	
	@Override
	public String executeAction(HttpServletRequest request) {
		request.setAttribute("title", TITLE);
		boolean postMethod = request.getMethod().equals("POST");
		
		Product product = null;
		String idStr = request.getParameter(PRODUCT_ID);
		
		try {
			try {
				int id = Integer.parseInt(idStr);
				product = DAOFactory.getProductDAO().find(id);
			} catch (Exception e) { request.setAttribute("reload", true); }
			
			if (!postMethod) {
				request.setAttribute("product", product);
			} else {
				System.out.println("EditProduct postMethod...");
				String title = request.getParameter(PRODUCT_TITLE);
				String description = request.getParameter(PRODUCT_DESCRIPTION);
				String img = request.getParameter(PRODUCT_IMG);
				String priceStr = request.getParameter(PRODUCT_PRICE);
				int idStore = Integer.parseInt(request.getParameter(PRODUCT_ID_STORE));
				int idProductType = Integer.parseInt(request.getParameter(PRODUCT_ID_PRODUCT_TYPE));


				priceStr = priceStr.replace(",", ".");
				double price = Double.parseDouble(priceStr);

				product.setTitle(title);
				product.setDescription(description);
				product.setImg(img);
				product.setPrice(price);
				product.setIdStore(idStore);
				product.setIdProductType(idProductType);
				DAOFactory.getProductDAO().update(product);
				
				request.getSession().setAttribute(INFO_ATTR, INFO_PRODUCT_UPDATED_OK);
				request.setAttribute("redirectUrl", ADMIN_PRODUCTS_LIST_URL);
			}
		}
		catch (Exception e) {
			if (postMethod) {
				request.getSession().setAttribute(INFO_ATTR, INFO_PRODUCT_UPDATED_FAIL);
				request.setAttribute("redirectUrl", ADMIN_PRODUCTS_EDIT_URL);
			}
			e.printStackTrace();
		}
		
		return JSP_PAGE;
	}

}
