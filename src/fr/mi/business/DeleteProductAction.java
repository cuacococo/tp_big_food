package fr.mi.business;


import javax.servlet.http.HttpServletRequest;

import fr.mi.model.dao.DAOFactory;
import fr.mi.web.util.AbstractAction;
import fr.mi.web.util.Constants;

public class DeleteProductAction extends AbstractAction {
	private static final String JSP_PAGE = Constants.JSP_PRODUCTS_NAME;
	
	@Override
	public String executeAction(HttpServletRequest request) {
		String idStr = request.getParameter("id");
		request.setAttribute("reload", true);
		
		if (idStr != null) {
			try {
				int id = Integer.parseInt(idStr);
				DAOFactory.getProductDAO().delete(id);
			}
			catch (Exception e) { e.printStackTrace(); }
		}
		
		return JSP_PAGE;
	}

}
