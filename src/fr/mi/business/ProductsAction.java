package fr.mi.business;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import fr.mi.model.beans.Product;
import fr.mi.model.dao.DAOFactory;
import fr.mi.web.util.AbstractAction;
import fr.mi.web.util.Constants;

public class ProductsAction extends AbstractAction {
	private static final String JSP_PAGE = Constants.JSP_PRODUCTS_NAME;
	private static final String TITLE = Constants.TITLE_PRODUCTS_PAGE;
	
	@Override
	public String executeAction(HttpServletRequest request) {
		request.setAttribute("title", TITLE);
		
		List<Product> products = DAOFactory.getProductDAO().findAll();
		request.setAttribute("products", products);
		
		return JSP_PAGE;
	}

}
